#!/usr/bin/bash

#getting holidays from the London Stock Exchange
if [[ -z $1 ]]
then
	echo "no stock exchange provided"
	exit 2
fi
stockExchange=$1
year=$(date +%Y)
URLName="https://www.stockmarketclock.com/exchanges/${stockExchange}/market-holidays/${year}"
SEHolidaysRaw=$(mktemp)
wget -O SEHolidaysRaw ${URLName}

tempNameFile=$(mktemp)
tempDateFile=$(mktemp)
cat SEHolidaysRaw | grep 'data-title="Name"' | sed -e "s/<[^>]*>//g;s/&#039;/'/g" | awk '$1=$1' > tempNameFile
cat SEHolidaysRaw | grep 'data-title="Observed Date"' | sed -e 's/<[^>]*>//g;s/,//g;s/&dagger;//' | awk '$1=$1' > tempDateFile
echo "Date,Reason" > ${stockExchange}${year}.csv
paste -d ',' tempDateFile tempNameFile >> ${stockExchange}${year}.csv
linenumber=1
while read line
do
	echo "${line}"
	linenumber=$((${linenumber}+1))
done < ${stockExchange}${year}.csv
