#!/usr/bin/bash

lineBehind=$1
if [[ -z ${lineBehind} ]]; then
	echo "please give a line number"
	exit
fi
targetFolder=$2
ls -la ${targetFolder} | tail -n ${lineBehind} | head -n 1

ls -la ~ | awk '{print substr($0, 4, 1)}' | grep -c "x"
