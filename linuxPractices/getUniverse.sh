#!/bin/bash
function usage {
	echo -e "usage: $0 VENDOR\neg: $0 NYSE\nValid VENDOR's are NYSE" >&2
	exit 1
}

test $# -eq 1||usage

VENDOR=$1
case $VENDOR in
  NYSE);;
  *)usage
esac
echo `date +%T.%N` starting

#TODO setup ftp site on the nas with some sample data
OUTDIR=/data/ref/uni/$VENDOR
#lftp -c open ftp://.....;get uni.`logname`

echo downloading universe for $VENDOR
#simulate a large file/slow connection by sleeping for 20 seconds, so that can see the value in running job in the background

LOGNAME=`logname`
FILE=uni.$LOGNAME
echo "downloaded $OUTDIR/$FILE"
