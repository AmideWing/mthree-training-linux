All files in this folder are framed as functions.

commonFunctions contains the functions useful to all other files. The function used to increment the version number is also in this file, called incrementVersion. It has the capability to handle multiple sets of sequences in the version numbering, e.g. <username>_1.10.5. To use this functionality, a third parameter of sequence set number is needed. It specifies which sequence set to increment, e.g. 1 to 2, 10 to 11 or 5 to 6.

newDevRelease and newDevReleaseAdvanced needs this third parameter in addition to the original META and PROJECT arguments as the version numbering would be affected when creating a new DEV release.

promoteDevToUat and promoteUatToProd do not need this third parameter as the version numbering is not affected.
