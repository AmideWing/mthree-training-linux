#~/usr/bin/bash

targetDirectory=$1
if [[ -z $targetDirectory ]]
then
	echo "no directory as first argument"
	exit 2
fi

for file in $(ls ${targetDirectory})
do
	mv ${targetDirectory}/${file} ${targetDirectory}/${file}.New
done
