#!usr/bin/bash
echo "first param is [ $1 ]"
echo "second param is [ $2 ]"
echo "third param is [ $3 ]"
echo "fourth param is [ $4 ]"
echo "show dollar at [ $@ ]"
echo "show dollar hash [ $# ]"

#copy over your parameters to meaningful names
meaningfulName=$1
anotherMeaningfulName=$2
#from here, do some work with the parameters
echo "$(meaningfulName) $(anotherMeaningfulName)":
