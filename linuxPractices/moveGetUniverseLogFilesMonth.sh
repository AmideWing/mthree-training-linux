#!/usr/bin/bash

ME=$USER
targetDirectory="/data/log/${ME}"
functionIdentifier="getUniverse.log"
allRelevantFileNames=$(ls ${targetDirectory}/*${functionIdentifier}*)
dateWithDash=$(echo "${allRelevantFileNames}" | awk -F '.' '{print $NF}' | sed -r 's/-[[:digit:]]{2}$//' | uniq)
for yearMonth in $(echo "${dateWithDash}")
do
	dateWithDot=$(echo ${yearMonth} | tr - .)
	targetMonthDirectory=${targetDirectory}/${dateWithDot}
	mkdir -p ${targetMonthDirectory}
	logFileMatch="${targetDirectory}/*${functionIdentifier}.${yearMonth}*"
	mv ${logFileMatch} ${targetMonthDirectory}
done
