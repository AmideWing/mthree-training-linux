#!/usr/bin/bash

psExtract=$(ps -eo pid,stat,cmd)
statusLabels="D R S T t W X Z"

for label in $statusLabels
do
	haveStatus=$(echo "${psExtract}" | awk -v awk_label=$label '$2 == awk_label' | head -1)
	if [[ -z ${haveStatus} ]]
	then
		echo "No process with label ${label} in current list of processes"
	else
		echo "${haveStatus} is an example of a process with label ${label}"
	fi
done
