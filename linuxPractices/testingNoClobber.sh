#!/usr/bin/bash

echo "This is the original text" > someFile.txt
cat someFile.txt

echo "I just overwrote the contents of the file" > someFile.txt
cat someFile.txt

set -o noclobber

echo "Let me overwrite the contents of the file again" > someFile.txt
cat someFile.txt

echo "But I can overwrite the contents of the file like this" >| someFile.txt
cat someFile.txt

echo "This is text in another file" > anotherFile.txt
cp anotherFile.txt someFile.txt
cat someFile.txt

echo "Overwrite times 2" >| someFile.txt
cat someFile.txt
cp -n anotherFile.txt someFile.txt
cat someFile.txt
